// clang-format off
include <utils/utils.scad>;
// clang-format on

/* [👁️ Display] */
arrangement = "🖨️ printable"; // [ "🖨️ printable", "📚 stacked" ]
slice_in_preview = false;
show_coin = false;

/* [Sorter] */
sorter_diameter = 100;         // [10:0.1:300]
sorter_thickness = 2;          // [0.1:0.1:10]
sorter_rim_height = 20;        // [1:0.1:100]
sorter_rim_holder_height = 10; // [1:0.1:100]
// single coin sorter element
coin = "1 €Cent"; // ["1 €Cent","2 €Cent","5 €Cent","10 €Cent","20 €Cent","50 €Cent","1 €","2 €","custom"]
// list of string coin names or diameters (e.g. ["1 €Cent","2 €Cent","2 €",30]) for stacked sorter
stacked_sorters = [];
// space between stacked sorters
sorter_clearance = 0.2; // [0:0.01:1]
// used if coin = "custom"
custom_coin_hole_diameter = 0; // [1:0.01:50]
// space around coins for the holes
coin_hole_clearance = 0.3; // [0:0.05:1]
// space between holes
coin_hole_distance = 1; // [1:0.1:50]
// whether a whole should be in the center
coin_hole_centered = true;
// allow partial holes
coin_holes_allow_partial = true;

coin_diameters = [
  // taken from Wikipedia.org
  [ "1 €Cent", 16.25 ],
  [ "2 €Cent", 18.75 ],
  [ "5 €Cent", 21.25 ],
  [ "10 €Cent", 19.75 ],
  [ "20 €Cent", 22.25 ],
  [ "50 €Cent", 24.25 ],
  [ "1 €", 23.25 ],
  [ "2 €", 25.75 ],
  [ "closed", 0 ],
  [ "custom", custom_coin_hole_diameter ],
];

get_coin_diameter = function(name) coin_diameters[search([name], coin_diameters, num_returns_per_match = 0)[0][0]][1];
get_diameter = function(coin) is_num(coin) ? coin : get_coin_diameter(coin);

coin_hole_diameter = get_coin_diameter(coin);

sorters = len(stacked_sorters) > 0 ? stacked_sorters : [coin];

// Precision
$fs = $preview ? 3 : 0.5;
$fa = $preview ? 3 : 0.5;
epsilon = 0.1 * 1; // to hide it from the Customizer

module
rim_shape()
{
  r = sorter_thickness / 2 * 0.99;
  offset_chain([ -r, r, r, -r ], chamfer = true)
  {
    square([ sorter_diameter / 2, sorter_thickness ]);
    translate([ sorter_diameter / 2 - sorter_thickness, 0 ]) square([ sorter_thickness, sorter_rim_height ]);
    translate([ sorter_diameter / 2 + sorter_clearance, sorter_rim_height ]) polygon([
      [ 0, 0 ],
      [ -sorter_clearance, 0 ],
      [
        -sorter_clearance,
        -max([ min([ 3 * sorter_thickness, sorter_rim_height - sorter_rim_holder_height ]), sorter_thickness ])
      ],
      [ sorter_thickness, 0 ],
      [ sorter_thickness, sorter_rim_holder_height ],
      [ 0, sorter_rim_holder_height ]
    ]);
  }
  square([ sorter_diameter / 2 - r, sorter_thickness ]);
}

module
siv(diameter, hole_center_distance)
{
  offset_fun = function(i, j, d)[j % 2 == 0 ? 0 : d / 2, 0];
  position_fun = function(i, j, d)[i * d, sin(60) * j * d + (coin_hole_centered ? d / sqrt(3) : 0)];
  decider_fun = function(i, j, d)(norm(position_fun(i, j, d) + offset_fun(i, j, d)) <
                                  diameter / 2 - (coin_holes_allow_partial ? d / 4 : d / 2));
  n = floor(diameter / hole_center_distance);
  mosaic(ix = [-n:n],
         iy = [-n:n],
         distance = hole_center_distance,
         position = position_fun,
         offset = offset_fun,
         decider = decider_fun) children();
}

module
sorter_siv(coin = coin,
           diameter = sorter_diameter,
           clearance = coin_hole_clearance,
           hole_distance = coin_hole_distance,
           hole_center_distance = undef,
           position = false)
{
  d = get_diameter(coin);
  echo(coin = coin, d = d);
  assert(is_num(d), str("Unknown coin: ", coin));
  siv(diameter = diameter,
      hole_center_distance = is_num(hole_center_distance) ? hole_center_distance : d + 2 * clearance + hole_distance)
  {
    if (position)
      children();
    else
      circle(d = d);
  }
}

module
sorter(coin = coin)
{
  difference()
  {
    rotate_extrude(convexity = 10) rim_shape();
    translate([ 0, 0, -epsilon / 2 ]) linear_extrude(sorter_thickness + epsilon, convexity = 5) intersection()
    {
      offset(coin_hole_clearance) sorter_siv(coin = coin);
      circle(d = sorter_diameter - 2 * coin_hole_clearance - 2 * sorter_thickness);
    }
  }
  if ($preview && show_coin)
    color("yellow") linear_extrude(sorter_thickness)
      sorter_siv(diameter = sorter_diameter * 0.75, coin = coin, position = true) circle(d = get_diameter(coin));
}

for (i = [0:len(sorters) - 1]) {
  c = sorters[i];
  translate(arrangement == "🖨️ printable" ? [ i * (sorter_diameter * 1.1), 0, 0 ]
                                               : [ 0, 0, i * (sorter_rim_height + sorter_clearance) ]) difference()
  {
    sorter(coin = c);
    if ($preview && slice_in_preview) {
      w = sorter_diameter / 2 + sorter_clearance + sorter_thickness + epsilon;
      translate([ 0, -w, -epsilon / 2 ]) cube([ w, w, sorter_rim_height + sorter_rim_holder_height + epsilon ]);
    }
  }
}
