# 🪙 Coin Sorter / Sorter for Round Things

This is an [OpenSCAD](https://openscad.org) design for a customizable tower of sorting sivs for round objects like coins and washers.

![coin sorter siv OpenSCAD render](https://gitlab.com/nobodyinperson/coin-sorter/-/raw/main/logo.png)

## 🔧 Customization

- get the code

  ```bash 
  git clone --recursive https://gitlab.com/nobodyinperson
  ```
- Open the `.scad` file with OpenSCAD
- Open the Customizer (Window ⮕ Customizer) and adjust the values to your liking
- Render by pressing **F6**, then save the `.stl` file with **F7**
